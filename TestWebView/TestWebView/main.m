//
//  main.m
//  TestWebView
//
//  Created by David Chhour on 25/10/16.
//  Copyright © 2016 Black Software. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
