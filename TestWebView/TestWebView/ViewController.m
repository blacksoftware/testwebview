//
//  ViewController.m
//  TestWebView
//
//  Created by David Chhour on 25/10/16.
//  Copyright © 2016 Black Software. All rights reserved.
//

#import "ViewController.h"

@interface ViewController ()

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    //wkwebkit
    //    WKWebViewConfiguration *theConfiguration = [[WKWebViewConfiguration alloc] init];
    //    WKWebView *webView = [[WKWebView alloc] initWithFrame:self.view.frame configuration:theConfiguration];
    //    webView.navigationDelegate = self;
    //    NSURL *nsurl=[NSURL URLWithString:@"https://sec.paymentexpress.com/pxmi3/EF4054F622D6C4C1B48E70070B3369BA52FFD2E7FAC3903B21CFD31331601602B7AB34968E8D8B220"];
    //    NSURLRequest *nsrequest=[NSURLRequest requestWithURL:nsurl];
    //    [webView loadRequest:nsrequest];
    //    [self.view addSubview:webView];
    
    //webView
    
    self.webView.delegate =  self;
    NSURL *nsurl=[NSURL URLWithString:@"https://sec.paymentexpress.com/pxmi3/EF4054F622D6C4C1B48E70070B3369BA54D6BC9BB0E1231D1FB07C614BA491F1D811868D904CD295B"];
    NSURLRequest *nsrequest=[NSURLRequest requestWithURL:nsurl];
    [self.webView loadRequest:nsrequest];
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)webView:(WKWebView *)webView didFailNavigation:(WKNavigation *)navigation withError:(NSError *)error
{
    if(error)
    {
        NSLog(@"Error loading webkit:%@", error.localizedDescription);
    }
}

-(void)webView:(UIWebView *)webView didFailLoadWithError:(NSError *)error
{
    if(error)
    {
        NSLog(@"Error loading webview:%@", error.localizedDescription);
    }
}

-(BOOL)webView:(UIWebView *)webView shouldStartLoadWithRequest:(NSURLRequest *)request navigationType:(UIWebViewNavigationType)navigationType
{
    NSMutableDictionary *queryStrings = [[NSMutableDictionary alloc] init];
    for (NSString *qs in [request.URL.query componentsSeparatedByString:@"&"]) {
        // Get the parameter name
        NSString *key = [[qs componentsSeparatedByString:@"="] objectAtIndex:0];
        // Get the parameter value
        NSString *value = [[qs componentsSeparatedByString:@"="] objectAtIndex:1];
        value = [value stringByReplacingOccurrencesOfString:@"+" withString:@" "];
        value = [value stringByReplacingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
        
        queryStrings[key] = value;
    }
    
    NSLog(@"Json dict:%@", queryStrings);
    if ([queryStrings objectForKey:@"result"])
    {
        NSString *message = [[NSString alloc] initWithFormat:@"<?xml version=\"1.0\" ?><ProcessResponse><PxPayUserId>DPSDemoSample_Dev</PxPayUserId><PxPayKey>Sample Key</PxPayKey><Response>%@</Response></ProcessResponse>", [queryStrings objectForKey:@"result"]];
        //NSURL * url = [NSURL URLWithString:@"https://uat.paymentexpress.com/pxaccess/pxpay.aspx"];
        NSURL * url = [NSURL URLWithString:@"https://sec.paymentexpress.com/pxaccess/pxpay.aspx"];
        NSMutableURLRequest * urlRequest = [NSMutableURLRequest requestWithURL:url];
        NSString *msgLength = [NSString stringWithFormat:@"%lu",(unsigned long)[message length]];
        
        [urlRequest addValue:@"application/xml; charset=utf-8" forHTTPHeaderField:@"Content-Type"];
        [urlRequest addValue:msgLength forHTTPHeaderField:@"Content-Length"];
        
        [urlRequest setHTTPMethod:@"POST"];
        [urlRequest setHTTPBody:[message dataUsingEncoding:NSUTF8StringEncoding]];
        
        NSURLConnection *conn = [[NSURLConnection alloc] initWithRequest:urlRequest delegate:self];
        
        if(conn) {
            NSLog(@"Connection Successful");
        } else {
            NSLog(@"Connection could not be made");
        }
        
        NSLog([NSString stringWithFormat:@"Post message: %%@"], message);
        
        // Creating a data task
        NSURLSessionDataTask *dataTask = [[NSURLSession sharedSession]
                                          dataTaskWithRequest:urlRequest completionHandler:^(NSData *data, NSURLResponse *response, NSError *error)
                                          {
                                              NSString *result = [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];
                                                  NSLog(@"Response from urlresponse: %@", result);
                                                  //here you get the response
                                          }];
        
        // Starting the task
        [dataTask resume];

    }
    
    //self.connection = [NSURLConnection connectionWithRequest:request delegate:self];
    return YES;
}


// This method is used to receive the data which we get using post method.
- (void)connection:(NSURLConnection *)connection didReceiveData:(NSData*)data
{
    NSLog(@"Received data from response: %@", data);
}

// This method receives the error report in case of connection is not made to server.
- (void)connection:(NSURLConnection *)connection didFailWithError:(NSError *)error
{
    if(error)
    {
        NSLog(@"Error loading webview:%@", error.localizedDescription);
    }
}

// This method is used to process the data after connection has made successfully.
- (void)connectionDidFinishLoading:(NSURLConnection *)connection
{
    NSLog(@"Finished loading connection");
}


@end
