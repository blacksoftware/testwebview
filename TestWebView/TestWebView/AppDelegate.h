//
//  AppDelegate.h
//  TestWebView
//
//  Created by David Chhour on 25/10/16.
//  Copyright © 2016 Black Software. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

