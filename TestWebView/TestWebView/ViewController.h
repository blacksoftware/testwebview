//
//  ViewController.h
//  TestWebView
//
//  Created by David Chhour on 25/10/16.
//  Copyright © 2016 Black Software. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <WebKit/WebKit.h>

@interface ViewController : UIViewController <WKUIDelegate, WKNavigationDelegate, WKScriptMessageHandler, UIWebViewDelegate>

@property (weak, nonatomic) IBOutlet UIWebView *webView;

@end

